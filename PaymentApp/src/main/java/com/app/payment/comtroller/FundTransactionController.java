package com.app.payment.comtroller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.payment.entity.FundTransaction;
import com.app.payment.entity.User;
import com.app.payment.service.FundTransactionService;
import com.app.payment.service.FundTransactnFeignClient;
import com.app.payment.service.UserService;
import com.thoughtworks.xstream.converters.time.LocalDateTimeConverter;

@RestController
@RequestMapping("/transactions")
@Validated
public class FundTransactionController {
	
	@Autowired
	private FundTransactionService fundTransactionService;
	
	@Autowired
	private FundTransactnFeignClient fundTransactnFeignClient;
	
	@Autowired
	private UserService userService;
	
	@GetMapping("{mobile}")
	public List<FundTransaction> getLatestTransactions(@PathVariable Long mobile){
		return fundTransactionService.findByMobile(mobile);
	}
	
	@PostMapping("/{fromMobile}/{toMobile}/{amount}")
    public ResponseEntity<String> transferFundByMobile(
    		@PathVariable("fromMobile") Long fromMobile,
    		@PathVariable("toMobile")  Long toMobile, 
    		@PathVariable("amount") Integer amount) throws Exception{
		
			ResponseEntity<String> responseEntity;
			List<User> user = null;
			String msg = "No user is registered with mobile number ";
			
			user = userService.findUsreByMobile(fromMobile);
			if(user==null) return new ResponseEntity<>(msg + fromMobile, HttpStatus.BAD_REQUEST );
			
			user = userService.findUsreByMobile(toMobile);
			if(user==null) return new ResponseEntity<>(msg + toMobile, HttpStatus.BAD_REQUEST );
			
			
			String status = fundTransactnFeignClient.transferFund(fromMobile, toMobile, amount);
			java.sql.Timestamp currentTimeStamp = java.sql.Timestamp.valueOf(LocalDateTime.now());
			
			if(StringUtils.equals(status, "Transferred Successfully")) fundTransactionService.saveTransaction(fromMobile, toMobile, amount, currentTimeStamp );
			
			responseEntity = new ResponseEntity<>(status, HttpStatus.CREATED );
			
			return responseEntity;
		
		
		
    }
}
