package com.app.payment.entity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="fund_transaction")
@Entity
public class FundTransaction {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
	private Integer id;
	
	@Column(name = "from_mobile")
	private Long fromMobile;
	
	@Column(name = "to_mobile")
	private Long toMobile;
	
	@Column(name = "amount")
	private Integer amount;
	
	@Column(name = "transaction_date")
	private Timestamp transactionDate;
	
	public FundTransaction() {
		// TODO Auto-generated constructor stub
	}
	
	public FundTransaction(Long fromMobile, Long toMobile, Integer amount, Timestamp transactionDate) {
		super();
		this.fromMobile = fromMobile;
		this.toMobile = toMobile;
		this.amount = amount;
		this.transactionDate = transactionDate;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getFromMobile() {
		return fromMobile;
	}

	public void setFromMobile(Long fromMobile) {
		this.fromMobile = fromMobile;
	}

	public Long getToMobile() {
		return toMobile;
	}

	public void setToMobile(Long toMobile) {
		this.toMobile = toMobile;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	
	public String getTransactionDate() {
		SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return  sdf.format(transactionDate);
	}

	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}
	
}
