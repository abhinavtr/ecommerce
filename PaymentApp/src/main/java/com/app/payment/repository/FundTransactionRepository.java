package com.app.payment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.payment.entity.FundTransaction;

@Repository
public interface FundTransactionRepository extends JpaRepository<FundTransaction, Integer>{

	List<FundTransaction> findFirst10ByFromMobile(Long mobile);

	List<FundTransaction> findFirst10ByFromMobileOrderByIdDesc(Long mobile);
	
}
