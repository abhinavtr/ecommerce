package com.app.payment.service;

import java.sql.Timestamp;
import java.util.List;

import com.app.payment.entity.FundTransaction;

public interface FundTransactionService {

	void saveTransaction(Long fromMobile, Long toMobile, Integer amount, Timestamp transactionDate);

	List<FundTransaction> findByMobile(Long mobile);
}
