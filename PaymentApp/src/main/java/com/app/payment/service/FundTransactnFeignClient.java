package com.app.payment.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "bankServiceFeign",url = "http://localhost:9092/bank-service/transaction")
public interface FundTransactnFeignClient {
	
		@PostMapping("/fund/transfer/{fromMobile}/{toMobile}/{amount}")
		String transferFund(@PathVariable Long fromMobile, @PathVariable Long toMobile, @PathVariable Integer amount);

}
