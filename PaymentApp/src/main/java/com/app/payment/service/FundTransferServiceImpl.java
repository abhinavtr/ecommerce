package com.app.payment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.payment.entity.FundTransaction;
import com.app.payment.entity.User;
import com.app.payment.repository.FundTransactionRepository;

@Service
public class FundTransferServiceImpl implements FundTransactionService{
	@Autowired
	FundTransactionRepository repo;
	@Autowired
	private UserService userService;
	
	@Override
	public void saveTransaction(Long fromMobile, Long toMobile, Integer amount, java.sql.Timestamp transactionDate) {
		FundTransaction fundTransaction = new FundTransaction(fromMobile, toMobile, amount, transactionDate);
		repo.save(fundTransaction);
		
	}
	
	@Override
	public List<FundTransaction> findByMobile(Long mobile) {
		return repo.findFirst10ByFromMobileOrderByIdDesc(mobile);
	}

}
