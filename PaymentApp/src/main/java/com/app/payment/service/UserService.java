package com.app.payment.service;

import java.util.List;
import java.util.Optional;

import com.app.payment.dto.UserDto;
import com.app.payment.dto.UserRequestDto;
import com.app.payment.dto.UserResponse;
import com.app.payment.entity.User;

public interface UserService {

	List<User> findUsreByMobile(Long mobile);
	
}
