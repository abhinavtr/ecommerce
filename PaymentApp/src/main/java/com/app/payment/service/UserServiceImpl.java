package com.app.payment.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.payment.entity.User;
import com.app.payment.repository.FundTransactionRepository;
import com.app.payment.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository repo;
	@Autowired
	FundTransactionRepository fundRepo;
	
	@Override
	public List<User> findUsreByMobile(Long mobile) {
		// TODO Auto-generated method stub
		return repo.findUserByMobile(mobile);
	}
}
