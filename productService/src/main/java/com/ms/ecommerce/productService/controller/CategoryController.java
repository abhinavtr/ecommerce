package com.ms.ecommerce.productService.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ms.ecommerce.productService.dto.CategoryResponse;
import com.ms.ecommerce.productService.service.CategoryService;

@RestController
@RequestMapping("/categories")
public class CategoryController {
	
	@Autowired
	public CategoryService categoryService;
	
	@GetMapping
	public ResponseEntity<List<CategoryResponse>> getAllCategory(){
		List<CategoryResponse> response = categoryService.findAll();
		ResponseEntity<List<CategoryResponse>> responseEntity = new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		return responseEntity;
	}
	
	@GetMapping("{name}")
	public ResponseEntity<List<CategoryResponse>> getCategoryByName(@PathVariable String name){
		List<CategoryResponse> response = categoryService.findByNameAll(name);
		ResponseEntity<List<CategoryResponse>> responseEntity = new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		return responseEntity;
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<CategoryResponse>> findByCategoryOrProductName(
			@RequestParam(value="category name", required = false) String categoryName, 
			@RequestParam (value="product name", required = false) String productName){
		List<CategoryResponse> response = categoryService.findByCategoryOrProductName(categoryName, productName);
		ResponseEntity<List<CategoryResponse>> responseEntity = new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		return responseEntity;
	}

}
