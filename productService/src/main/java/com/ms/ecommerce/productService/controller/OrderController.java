package com.ms.ecommerce.productService.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ms.ecommerce.productService.dto.OrderRequestDto;
import com.ms.ecommerce.productService.dto.OrderResponse;
import com.ms.ecommerce.productService.entity.Customer;
import com.ms.ecommerce.productService.entity.OrderDetail;
import com.ms.ecommerce.productService.entity.Product;
import com.ms.ecommerce.productService.service.CustomerService;
import com.ms.ecommerce.productService.service.FundTransferFeignClient;
import com.ms.ecommerce.productService.service.OrderService;
import com.ms.ecommerce.productService.service.ProductService;

@RestController
@RequestMapping("/orders")
public class OrderController {
	
	@Autowired
	public OrderService orderService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	FundTransferFeignClient fundTransferFeignClient;
	
	/*
	 * @GetMapping("/orderByName") public ResponseEntity<List<OrderResponse>>
	 * getAllOrdersByCustomerName(@RequestParam String name){ List<OrderResponse>
	 * response = orderService.findByCustomerFirstName(name);
	 * ResponseEntity<List<OrderResponse>> responseEntity = new
	 * ResponseEntity<>(response, HttpStatus.ACCEPTED); return responseEntity; }
	 */
	
	@GetMapping("/{customerId}")
	public ResponseEntity<List<OrderResponse>> getOrderyById(@PathVariable Integer customerId){
		List<OrderResponse> response = orderService.findByCustomerId(customerId);
		ResponseEntity<List<OrderResponse>> responseEntity = new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		return responseEntity;
	}
	
	@PostMapping("/placeOrder")
	public ResponseEntity<OrderResponse> createOrder(@RequestBody OrderRequestDto orderDto){
		
		String message = null; 
		ResponseEntity<OrderResponse> responseEntity = null;
		if(orderDto.getCustomerId() == null || orderDto.getProductName() == null || orderDto.getQuantity()==null) {
			message="CustomerId, ProductName, Quantity are mandatory";
			OrderResponse response = new OrderResponse();
			response.setMessage(message);
			responseEntity = new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
			return responseEntity;
		}
		
		Product product = productService.getProductByproductName(orderDto.getProductName());
		System.out.println("==========="+orderDto.getCustomerId());
		//Customer customer = customerService.getCustomerById(orderDto.getCustomerId());
		Optional<Customer> customer = customerService.findCustomerById(orderDto.getCustomerId());
		System.out.println("==========="+customer.get().getFirstName());
		
		OrderDetail newOrder = new OrderDetail();
		BeanUtils.copyProperties(orderDto, newOrder);

		//Todo: If customer not present, still we can sell the product 
		if(customer!=null)  newOrder.setCustomerId(customer.get().getId());
		
		//If account number is not present in request then take it from customer table
		if(orderDto.getClientAccountNumber() == null) newOrder.setClientAccountNumber(customer.get().getAccountNumber());
		
		List<Product> products = new ArrayList<>();
		products.add(product);
		newOrder.setProducts(products);
		
		newOrder.setTotal(product.getProductPrice() * orderDto.getQuantity());
		
		String msg = fundTransferFeignClient.transferFund(newOrder.getClientAccountNumber(), 5280179956240896L, newOrder.getTotal());
		System.out.println("Fund Transfer msg = "+ msg);
		
		OrderResponse response = null;
		if(StringUtils.isNotEmpty(msg) && StringUtils.equals(msg, "Transferred Successfully")) {
			response = orderService.save(newOrder);
			return new ResponseEntity<>(response, HttpStatus.CREATED);
		}
		
		response= new OrderResponse();
		response.setMessage("Order could not be placed due to unsuccessful fund transfer");
		return new ResponseEntity<>(response, HttpStatus.EXPECTATION_FAILED);
	}

}
