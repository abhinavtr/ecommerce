package com.ms.ecommerce.productService.dto;

import java.util.List;

import com.ms.ecommerce.productService.entity.Product;


public class CategoryResponse {
	
	private Integer id;

	private String categoryName;
	
	private String categoryId;
	
	private List<Product> products;
	
	public CategoryResponse() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	
}
