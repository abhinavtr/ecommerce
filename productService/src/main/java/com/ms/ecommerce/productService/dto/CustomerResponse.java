package com.ms.ecommerce.productService.dto;

import java.util.List;

import com.ms.ecommerce.productService.entity.OrderDetail;

public class CustomerResponse {
	
		private Integer id;
		
		private String firstName;
		
		private String lastName;
		
		private String addr;
		
		private String email;
		
		private Long accountNumber;
		
		private List<OrderDetail> orders;
		
		public CustomerResponse() {
			
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getAddr() {
			return addr;
		}

		public void setAddr(String addr) {
			this.addr = addr;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public Long getAccountNumber() {
			return accountNumber;
		}

		public void setAccountNumber(Long accountNumber) {
			this.accountNumber = accountNumber;
		}

		public List<OrderDetail> getOrders() {
			return orders;
		}

		public void setOrders(List<OrderDetail> orders) {
			this.orders = orders;
		}
		
		
		
		
}
