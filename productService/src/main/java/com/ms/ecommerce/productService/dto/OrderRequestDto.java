package com.ms.ecommerce.productService.dto;

public class OrderRequestDto {
	
	private String productName;
	
	private Integer customerId;
	
	private Integer quantity;
	
	private Long clientAccountNumber;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getClientAccountNumber() {
		return clientAccountNumber;
	}

	public void setClientAccountNumber(Long clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}
	
	
	
}
