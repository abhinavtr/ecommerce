package com.ms.ecommerce.productService.dto;

import java.sql.Timestamp;
import java.util.List;

import com.ms.ecommerce.productService.entity.Customer;
import com.ms.ecommerce.productService.entity.Product;


public class OrderResponse {
	private String message;
	
	private Integer id;
	
	private Long orderNumber;
	
	private List<Product> products;
	
	private Customer customer;
	
	private Integer quantity;
	
	private Integer total;
	
	private Timestamp transactionDate;
	
	private Long clientAccountNumber;
	
	
	public OrderResponse() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Long orderNumber) {
		this.orderNumber = orderNumber;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Timestamp getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getClientAccountNumber() {
		return clientAccountNumber;
	}

	public void setClientAccountNumber(Long clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	
	
}
