package com.ms.ecommerce.productService.dto;

public class ProductResponse {
	
	private String productName;
	
	private String productId;
	
	private String productPrice;
	
	public ProductResponse() {
		
	}
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}
	
	

}
