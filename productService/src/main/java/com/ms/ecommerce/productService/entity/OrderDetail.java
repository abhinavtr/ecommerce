package com.ms.ecommerce.productService.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Table(name="order_detail")
@Entity
public class OrderDetail {
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
	private Integer id;
	
	@Column(name = "order_number")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long orderNumber;
	
	@Column(name = "product")
	@OneToMany(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL, targetEntity = Product.class)
	private List<Product> products;
	
	/*
	 * @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity =
	 * Customer.class)
	 * 
	 * @JoinColumn(name = "id", insertable = false, updatable = false)
	 */
	@Column(name = "customer_id")
	private Integer customerId;
	
	@Column(name = "quantity")
	private Integer quantity;
	
	@Column(name = "total")
	private Integer total;
	
	@Column(name = "transaction_date")
	private Timestamp transactionDate;
	
	@Column(name = "client_account_number")
	private Long clientAccountNumber;
	
	public OrderDetail() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Long orderNumber) {
		this.orderNumber = orderNumber;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Timestamp getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getClientAccountNumber() {
		return clientAccountNumber;
	}

	public void setClientAccountNumber(Long clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	
	
	
	
}
