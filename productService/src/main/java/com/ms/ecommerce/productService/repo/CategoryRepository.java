package com.ms.ecommerce.productService.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ms.ecommerce.productService.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>{

	List<Category> findByCategoryName(String name);

	List<Category> findByCategoryNameOrProductsProductName(String categoryName, String productName);


}
