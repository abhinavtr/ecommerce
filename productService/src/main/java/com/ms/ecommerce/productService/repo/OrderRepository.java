package com.ms.ecommerce.productService.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ms.ecommerce.productService.entity.Category;
import com.ms.ecommerce.productService.entity.OrderDetail;

@Repository
public interface OrderRepository extends JpaRepository<OrderDetail, Integer>{

	List<OrderDetail> findByCustomerId(Integer customerId);


}
