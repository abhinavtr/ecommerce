package com.ms.ecommerce.productService.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ms.ecommerce.productService.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{

	Product findProductByProductName(String productName);

}
