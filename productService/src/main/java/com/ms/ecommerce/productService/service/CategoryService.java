package com.ms.ecommerce.productService.service;

import java.util.List;

import com.ms.ecommerce.productService.dto.CategoryResponse;

public interface CategoryService {

	List<CategoryResponse> findAll();

	List<CategoryResponse> findByNameAll(String name);

	List<CategoryResponse> findByCategoryOrProductName(String categoryName, String productName);


}
