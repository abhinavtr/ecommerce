package com.ms.ecommerce.productService.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms.ecommerce.productService.dto.CategoryResponse;
import com.ms.ecommerce.productService.entity.Category;
import com.ms.ecommerce.productService.repo.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService{
	
	@Autowired
	CategoryRepository categoryRepo;

	@Override
	public List<CategoryResponse> findAll() {
		List<Category> categories = categoryRepo.findAll();
		
		List<CategoryResponse> response = new ArrayList<>();
		for(Category category : categories) {
			CategoryResponse categoryResponse = new CategoryResponse();
			BeanUtils.copyProperties(category, categoryResponse);
			response.add(categoryResponse);
		}
		return response;
	}

	@Override
	public List<CategoryResponse> findByNameAll(String name) {
		List<Category> categories = categoryRepo.findByCategoryName(name);
		
		List<CategoryResponse> response = new ArrayList<>();
		for(Category category : categories) {
			CategoryResponse categoryResponse = new CategoryResponse();
			BeanUtils.copyProperties(category, categoryResponse);
			response.add(categoryResponse);
		}
		return response;
	}

	@Override
	public List<CategoryResponse> findByCategoryOrProductName(String categoryName, String productName) {
		List<Category> categories = categoryRepo.findByCategoryNameOrProductsProductName(categoryName, productName);
		
		List<CategoryResponse> response = new ArrayList<>();
		for(Category category : categories) {
			CategoryResponse categoryResponse = new CategoryResponse();
			BeanUtils.copyProperties(category, categoryResponse);
			response.add(categoryResponse);
		}
		return response;
	}
	
	
	
}
