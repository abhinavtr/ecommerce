package com.ms.ecommerce.productService.service;

import java.util.Optional;

import com.ms.ecommerce.productService.entity.Customer;

public interface CustomerService{

	Customer getCustomerById(Integer customerId);
	
	Optional<Customer> findCustomerById(Integer customerId);

}
