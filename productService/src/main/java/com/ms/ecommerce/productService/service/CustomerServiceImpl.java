package com.ms.ecommerce.productService.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms.ecommerce.productService.entity.Customer;
import com.ms.ecommerce.productService.repo.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService{
	
	@Autowired
	CustomerRepository customerRepository;

	@Override
	public Customer getCustomerById(Integer id) {
		return customerRepository.getOne(id);
	}

	@Override
	public Optional<Customer> findCustomerById(Integer id) {
		return customerRepository.findById(id);
	}

}
