package com.ms.ecommerce.productService.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "fundTransfer", url = "http://localhost:9092/bank-service/users")
public interface FundTransferFeignClient {
	@PostMapping("/fund/transfer/{from}/{to}/{amount}")
    public String transferFund(@PathVariable Long from, @PathVariable Long to, @PathVariable Integer amount);
	
}
