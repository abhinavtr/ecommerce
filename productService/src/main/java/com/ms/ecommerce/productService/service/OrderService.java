package com.ms.ecommerce.productService.service;

import java.util.List;

import com.ms.ecommerce.productService.dto.OrderRequestDto;
import com.ms.ecommerce.productService.dto.OrderResponse;
import com.ms.ecommerce.productService.entity.OrderDetail;

public interface OrderService{


	List<OrderResponse> findByCustomerId(Integer id);
	
	OrderResponse save(OrderDetail newOrder);

}
