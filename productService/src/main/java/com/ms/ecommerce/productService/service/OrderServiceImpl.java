package com.ms.ecommerce.productService.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ms.ecommerce.productService.dto.OrderRequestDto;
import com.ms.ecommerce.productService.dto.OrderResponse;
import com.ms.ecommerce.productService.entity.OrderDetail;
import com.ms.ecommerce.productService.entity.Product;
import com.ms.ecommerce.productService.repo.OrderRepository;

@Service
public class OrderServiceImpl implements OrderService{
	
	@Autowired
	OrderRepository orderRepository;
	@Autowired
	ProductService productService;


	@Override
	public List<OrderResponse> findByCustomerId(Integer customerId) {
		List<OrderDetail> orders = orderRepository.findByCustomerId(customerId);
		
		List<OrderResponse> response = new ArrayList<>();
		for(OrderDetail order : orders) {
			OrderResponse orderResponse = new OrderResponse();
			BeanUtils.copyProperties(order, orderResponse);
			response.add(orderResponse);
		}
		return response;
	}

	@Override
	@Transactional(readOnly=false)
	public OrderResponse save(OrderDetail newOrder) {//productName; customerId; quantity; clientAccountNumber
	
		/*
		 * Order newOrder = new Order(); Be
		 *anUtils.copyProperties(order, newOrder);
		*/
		/*
		 * Product product =
		 * productService.getProductByproductName(order.getProductName());
		 * newOrder.setTotal(product.getProductPrice() * newOrder.getQuantity());
		 */

		/*
		 * List<Product> products = new ArrayList<>(); products.add(product);
		 * newOrder.setProducts(products);
		 */
		
		java.sql.Timestamp currentTimeStamp = java.sql.Timestamp.valueOf(LocalDateTime.now());
		newOrder.setTransactionDate(currentTimeStamp);
		
		long number = (long) (Math.random() * 10000L);
		newOrder.setOrderNumber(number);
		
		OrderDetail savedOrder = orderRepository.save(newOrder);
		
		OrderResponse orderResponse = new OrderResponse();
		BeanUtils.copyProperties(savedOrder, orderResponse);
		orderResponse.setMessage("Order Placed with order# "+ savedOrder.getOrderNumber());
		return orderResponse;
	}

}
