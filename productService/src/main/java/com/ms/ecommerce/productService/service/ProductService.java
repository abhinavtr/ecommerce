package com.ms.ecommerce.productService.service;

import com.ms.ecommerce.productService.entity.Product;

public interface ProductService{

	Product getProductByproductName(String productName);

}
