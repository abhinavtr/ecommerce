package com.ms.ecommerce.productService.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms.ecommerce.productService.entity.Product;
import com.ms.ecommerce.productService.repo.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	ProductRepository productRepository;

	@Override
	public Product getProductByproductName(String productName) {
		
		return productRepository.findProductByProductName(productName);
	}

}
