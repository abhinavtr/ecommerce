package com.example.wawademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableEurekaClient
@EnableJpaRepositories(basePackages = "com.example.wawademo.repository")
public class WawademoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WawademoApplication.class, args);
	}

}
