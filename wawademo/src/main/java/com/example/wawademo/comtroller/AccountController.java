package com.example.wawademo.comtroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.wawademo.entity.Account;
import com.example.wawademo.service.AccountService;

@RestController
@RequestMapping("/account")
public class AccountController {
	@Autowired
	private AccountService accountService;
	
	@GetMapping
	public List<Account> getAllAccount(){
		return accountService.getAll();
	}
	
	@PostMapping("/delete/{id}")
	public String deleteEmpById(@PathVariable Integer id) {
		
		accountService.deleteAccountById(id);
		return "account deleted successfully";
		
	}
}
