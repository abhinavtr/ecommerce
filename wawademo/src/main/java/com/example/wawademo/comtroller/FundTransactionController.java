package com.example.wawademo.comtroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.wawademo.dto.UserDto;
import com.example.wawademo.dto.UserResponse;
import com.example.wawademo.entity.FundTransaction;
import com.example.wawademo.service.FundTransactionService;
import com.example.wawademo.service.UserService;

@RestController
@RequestMapping("/transaction")
public class FundTransactionController {
	
	@Autowired
	private FundTransactionService fundTransactionService;
	@Autowired
	private UserService userService;
	
	
	@GetMapping("{account}")
	public List<FundTransaction> getAllTransactions(@PathVariable Long account){
		return fundTransactionService.allTransactions(account);
	}
	
	@GetMapping("debits/{account}")
	public List<FundTransaction> getAllDebits(@PathVariable Long account){
		return fundTransactionService.allDebits(account);
	}
	
	@GetMapping("credits/{account}")
	public List<FundTransaction> getAllCredits(@PathVariable Long account){
		return fundTransactionService.allCredits(account);
	}
	
	@GetMapping("statement/{account}/{months}")
	public List<FundTransaction> getMonthlyReport(@PathVariable Long account, @PathVariable Integer months){
		return fundTransactionService.getMonthlyReport(account, months);
	}
	
	@GetMapping("statement/{account}/{month}/{year}")
	public List<FundTransaction> getMonthlyReport(@PathVariable Long account, @PathVariable Integer month, @PathVariable Integer year){
		return fundTransactionService.getMonthlyReport(account, month, year);
	}
	
	@PostMapping("/fund/transfer/{fromMobile}/{toMobile}/{amount}")
    public ResponseEntity<String> transferFundByMobile(@PathVariable("fromMobile") Long fromMobile,@PathVariable("toMobile")  Long toMobile, @PathVariable("amount") Integer amount) {
		
		ResponseEntity<String> responseEntity;
		
		if(fromMobile == null || toMobile==null || amount == null || amount<1) 
			return new ResponseEntity<>("from/to/amount are mandatory and amount should be greater than 0", HttpStatus.BAD_REQUEST ); 
		
		UserResponse userResponseFrom = userService.getUserByMobile(fromMobile);
		if(userResponseFrom.getUsersDto() == null) 
			return new ResponseEntity<>(userResponseFrom.getStatusMessage(), HttpStatus.BAD_REQUEST ); 
		
		UserResponse userResponseTo = userService.getUserByMobile(toMobile);
		if(userResponseTo.getUsersDto() == null) 
			return new ResponseEntity<>(userResponseTo.getStatusMessage(), HttpStatus.BAD_REQUEST ); 
		
		String status = userService.transferFund(userResponseFrom.getUsersDto().get(0).getAccounts().get(0).getAccountNumber(), 
				userResponseTo.getUsersDto().get(0).getAccounts().get(0).getAccountNumber(), amount);
		
		responseEntity = new ResponseEntity<>(status, HttpStatus.CREATED );
		return responseEntity;
		
		
		
    }
}
