package com.example.wawademo.comtroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.wawademo.dto.UserResponse;
import com.example.wawademo.dto.UserRequestDto;
import com.example.wawademo.dto.UserDto;
import com.example.wawademo.entity.User;
import com.example.wawademo.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	@Autowired
	private UserService userService;
	
	@GetMapping
	public UserResponse getAllUsers(){
		UserResponse usersResponse  = userService.getAll();
		usersResponse.setStatusCode(HttpStatus.CREATED);
		return usersResponse;
	}
	
	@GetMapping("{name}")
	public ResponseEntity<List<UserDto>> getAllUsers(@PathVariable String name){
		if(name==null) return null;
		List<UserDto> userResponse  = userService.findByFirstName(name);
		ResponseEntity<List<UserDto>> responseEntity = new ResponseEntity<>(userResponse, HttpStatus.ACCEPTED );
		return responseEntity;
	}
	
	@PostMapping
	public User addUser(@RequestBody UserRequestDto userDto) {
		if(userDto==null) return null;
		User addedUser = userService.saveUser(userDto);
		return addedUser;
	}
	
	@PostMapping("/fund/transfer/{from}/{to}/{amount}")
    public String transferFund(@PathVariable("from") Long from,@PathVariable("to")  Long to, @PathVariable("amount") Integer amount) {
		if(from==null || to==null || amount==null) return null;
    	return userService.transferFund(from, to, amount);
    }
	
	@PostMapping("/delete/{id}")
	public String deleteEmpById(@PathVariable Integer id) {
		if(id==null) return null;
		
		userService.deleteUserById(id);
		return "employee deleted successfully";
	}
	
	@GetMapping("/account/{account}")
	public User getUserByAccountsNumber(@PathVariable Long account) {
		if(account==null) return null;
		
		return userService.getUserByAccountNumber(account);
	}
	
}
