package com.example.wawademo.dto;

import java.util.List;

import org.springframework.http.HttpStatus;

public class AnotherUserResponse {
	private HttpStatus statusCode;
	private String statusMessage;
	List<UserResponse> userResponse;
	
	public HttpStatus getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public List<UserResponse> getUserResponse() {
		return userResponse;
	}
	public void setUserResponse(List<UserResponse> userResponse) {
		this.userResponse = userResponse;
	}
	
	
}
