package com.example.wawademo.dto;

import java.util.List;

import org.springframework.http.HttpStatus;

public class UserResponse {
	private HttpStatus statusCode;
	private String statusMessage;
	List<UserDto> usersDto;
	private Long mobile;
	
	
	
	public UserResponse(HttpStatus statusCode, String statusMessage, List<UserDto> usersDto) {
		super();
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
		this.usersDto = usersDto;
	}
	
	public HttpStatus getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public List<UserDto> getUsersDto() {
		return usersDto;
	}
	public void setUsersDto(List<UserDto> userResponse) {
		this.usersDto = userResponse;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	
	
	
}
