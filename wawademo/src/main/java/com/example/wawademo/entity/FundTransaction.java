package com.example.wawademo.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="fund_transaction")
@Entity
public class FundTransaction {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
	private Integer id;
	
	@Column(name = "from_account")
	private Long fromAccount;
	
	@Column(name = "to_account")
	private Long toAccount;
	
	@Column(name = "amount")
	private Integer amount;
	
	@Column(name = "transaction_date")
	private Timestamp transactionDate;
	
	public FundTransaction() {
		// TODO Auto-generated constructor stub
	}
	
	public FundTransaction(Long fromAccount, Long toAccount, Integer amount, Timestamp transactionDate) {
		super();
		this.fromAccount = fromAccount;
		this.toAccount = toAccount;
		this.amount = amount;
		this.transactionDate = transactionDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(Long fromAccount) {
		this.fromAccount = fromAccount;
	}

	public Long getToAccount() {
		return toAccount;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public void setToAccount(Long toAccount) {
		this.toAccount = toAccount;
	}

	public Timestamp getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	
}
