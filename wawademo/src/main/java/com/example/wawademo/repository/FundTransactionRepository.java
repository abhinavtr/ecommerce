package com.example.wawademo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.wawademo.entity.FundTransaction;

@Repository
public interface FundTransactionRepository extends JpaRepository<FundTransaction, Integer>{
	public List<FundTransaction> findByFromAccountOrToAccount(Long fromAccount, Long toAccount);

	public List<FundTransaction> findByFromAccount(Long fromAccount);
	public List<FundTransaction> findByToAccount(Long toAccount);

	public List<FundTransaction> findByFromAccountOrToAccountAndTransactionDateBeforeAndTransactionDateAfter(
			Long account, Long account2, Timestamp toTimeStamp, Timestamp fromTimeStamp);
	
}
