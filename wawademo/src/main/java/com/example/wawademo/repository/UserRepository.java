package com.example.wawademo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.wawademo.entity.User;


@Repository
public interface UserRepository extends JpaRepository<User, Integer>, UserRepositoryCustom{

	User getUserByAccountsAccountNumber(Long accountNumber);

	List<User> findByFirstName(String firstName);

	Optional<List<User>> findUserByMobile(Long mobile);

}
