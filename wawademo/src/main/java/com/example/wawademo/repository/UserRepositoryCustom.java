package com.example.wawademo.repository;

public interface UserRepositoryCustom {
	Integer transferFund(Long fromAccountNumber, Integer fromBalance, Long toAccountNumber, Integer toBalance);
}
