package com.example.wawademo.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.wawademo.entity.Account;

@Repository
@Transactional(readOnly=false)
public class UserRepositoryImpl implements UserRepositoryCustom{ 
	
	@PersistenceContext
    EntityManager entityManager;

	

	@Override
	@Modifying
	public Integer transferFund(Long fromAccountNumber, Integer fromBalance, Long toAccountNumber, Integer toBalance) {
			
		Query query = entityManager.createNativeQuery("update Account as account set  balance=" + fromBalance +
	                " WHERE account_number=?", Account.class);
	        query.setParameter(1, fromAccountNumber);
	        int count1 = query.executeUpdate();
	        
	        int count2 = 0;
	        if(count1>0){
	        query = entityManager.createNativeQuery("update Account as account set  balance=" + toBalance +
	                " WHERE account_number=?", Account.class);
	        query.setParameter(1, toAccountNumber);
	        count2 = query.executeUpdate();
	        }
	        if(count1>0 && count2>0) {
	        	return count1;
	        }
        
        return 0;
        
	}

	
	

}

