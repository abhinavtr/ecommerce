package com.example.wawademo.service;

import java.util.List;

import com.example.wawademo.entity.Account;

public interface AccountService {
	//void createAccount(Account account);
	Account createAccount();
	
	List<Account> getAll();
	List<Account> findByAccountNumber(Long accountNumber);
	//List<Account> findByUserId(Integer userId);
	
	
	String deleteAccountById(Integer id);
}
