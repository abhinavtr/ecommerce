package com.example.wawademo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.wawademo.entity.Account;
import com.example.wawademo.repository.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService{

	@Autowired
	AccountRepository repo;

	@Override
	public Account createAccount() {
		Account account = new Account();
		account.setBalance(10000);
		account.setActive(true);
		
		long first14 = (long) (Math.random() * 100000000000000L);
		long number = 5200000000000000L + first14;
		account.setAccountNumber(number);	//16 digit account number starting with 52
		
		return account;
	}

	@Override
	public List<Account> getAll() {
		return repo.findAll();
	}

	@Override
	public String deleteAccountById(Integer id) {
		return null;
	}

	@Override
	public List<Account> findByAccountNumber(Long accountNumber) {
		return repo.findByAccountNumber(accountNumber);
	}

	/*
	 * @Override public List<Account> findByUserId(Integer userId) { return
	 * repo.findByUserId(userId); }
	 */

}
