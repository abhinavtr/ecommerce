package com.example.wawademo.service;

import java.util.List;

import com.example.wawademo.entity.FundTransaction;

public interface FundTransactionService {
	List<FundTransaction> allDebits(Long fromAccount);
	List<FundTransaction> allCredits(Long toAccount);
	List<FundTransaction> allTransactions(Long account);
	List<FundTransaction> getMonthlyReport(Long account, Integer month);
	List<FundTransaction> getMonthlyReport(Long account, Integer month, Integer year);
}
