package com.example.wawademo.service;

import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.wawademo.entity.FundTransaction;
import com.example.wawademo.repository.FundTransactionRepository;

@Service
public class FundTransferServiceImpl implements FundTransactionService{
	@Autowired
	FundTransactionRepository repo;
	@Autowired
	private UserService userService;
	
	@Override
	public List<FundTransaction> allDebits(Long fromAccount) {
		return repo.findByFromAccount(fromAccount);
	}

	@Override
	public List<FundTransaction> allCredits(Long toAccount) {
		return repo.findByToAccount(toAccount);
	}

	@Override
	public List<FundTransaction> allTransactions(Long account) {
		return repo.findByFromAccountOrToAccount(account,account);
	}

	
	  @Override 
	  public List<FundTransaction> getMonthlyReport(Long account,Integer month) {
		  
		  LocalDateTime currentDateTime = LocalDateTime.now();
		  LocalDateTime fromDate = currentDateTime.minusMonths(month);
		  java.sql.Timestamp fromTimeStamp = java.sql.Timestamp.valueOf(fromDate);
		  java.sql.Timestamp toTimeStamp = java.sql.Timestamp.valueOf(currentDateTime);
		  
		  return repo.findByFromAccountOrToAccountAndTransactionDateBeforeAndTransactionDateAfter(account, account, toTimeStamp,fromTimeStamp) ;
	  }

	@Override
	public List<FundTransaction> getMonthlyReport(Long account, Integer month, Integer year) {
		LocalDateTime fromDate = LocalDateTime.of(year, month, 1, 00, 00);
		
		YearMonth yearMonthObject = YearMonth.of(year, month);
		LocalDateTime toDate = LocalDateTime.of(year, month, yearMonthObject.lengthOfMonth(), 23, 59);
		
		java.sql.Timestamp fromTimeStamp = java.sql.Timestamp.valueOf(fromDate);
		java.sql.Timestamp toTimeStamp = java.sql.Timestamp.valueOf(toDate);
		
		//LocalDateTime localDateTime = toTimeStamp.toLocalDateTime();
		return repo.findByFromAccountOrToAccountAndTransactionDateBeforeAndTransactionDateAfter(account, account, toTimeStamp,fromTimeStamp) ;
		 
	}

}
