package com.example.wawademo.service;

import java.util.List;

import com.example.wawademo.dto.UserResponse;
import com.example.wawademo.dto.UserRequestDto;
import com.example.wawademo.dto.UserDto;
import com.example.wawademo.entity.User;

public interface UserService {

	User saveUser(UserRequestDto userDto);

	UserResponse getAll();
	
	List<UserDto> findByFirstName(String firstName);

	String deleteUserById(Integer id);

	String transferFund(Long userid1, Long userid2, Integer amount);
	
	User getUserByAccountNumber(Long accounts);

	UserResponse getUserByMobile(Long Mobile);

}
