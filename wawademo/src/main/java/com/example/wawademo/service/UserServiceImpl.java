package com.example.wawademo.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.wawademo.dto.UserResponse;
import com.example.wawademo.dto.UserRequestDto;
import com.example.wawademo.dto.UserDto;
import com.example.wawademo.entity.Account;
import com.example.wawademo.entity.FundTransaction;
import com.example.wawademo.entity.User;
import com.example.wawademo.repository.FundTransactionRepository;
import com.example.wawademo.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository repo;
	@Autowired
	FundTransactionRepository fundRepo;
	
	@Autowired
	AccountService accountService;

	@Override
	public User saveUser(UserRequestDto userDto) {
		try {
			User user = new User();
			BeanUtils.copyProperties(userDto, user);
			
			Account account = accountService.createAccount();
			List<Account> accounts = new ArrayList<Account>();
			accounts.add(account);
			user.setAccounts(accounts);
			
			
			long first4 = (long) (Math.random() * 10000L);
			long number = 52000L + first4;
			user.setUserName(userDto.getFirstName()+number);
			user.setPassword(userDto.getLastName()+number);
			
			return repo.save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public UserResponse getAll() {
		List<User> users = repo.findAll();
		List<UserDto> usersResponse = new ArrayList<>();
		for(User user : users) {
			UserDto userDto = new UserDto();
			BeanUtils.copyProperties(user, userDto);
			usersResponse.add(userDto);
		}
		
		UserResponse response = new UserResponse(HttpStatus.OK, "User Found", usersResponse);
		response.setUsersDto(usersResponse);
		
		return response;
	}

	@Override
	public String deleteUserById(Integer id) {
		repo.deleteById(id);
		return "User with id=" + id + " deleted successfully";
	}

	@Override
	public List<UserDto> findByFirstName(String firstName) {
		try {
			List<User> users = repo.findByFirstName(firstName);
			List<UserDto> list = new ArrayList<>();
			
			for(User user : users) {
				UserDto userResponse = new UserDto();
				BeanUtils.copyProperties(user, userResponse);
				list.add(userResponse);
			}
			
			
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public String transferFund(Long fromAccountNumber, Long toAccountNumber, Integer amount) {
		Integer i = 0;
		try {
			User fromUser = repo.getUserByAccountsAccountNumber(fromAccountNumber);
			User toUser = repo.getUserByAccountsAccountNumber(toAccountNumber);
			
			Account fromAccount = fromUser.getAccounts().get(0);
			Account toAccount = toUser.getAccounts().get(0);
			if(fromAccount.getBalance() < amount )  return "Transfer declined due to low balance";
			
			i = fromAccount.getBalance()>amount ? repo.transferFund( fromAccountNumber, fromAccount.getBalance()-amount, toAccountNumber, toAccount.getBalance()+amount) : 0;
			
			java.sql.Timestamp currentTimeStamp = java.sql.Timestamp.valueOf(LocalDateTime.now());
			if (i>0) fundRepo.save(new FundTransaction(fromAccountNumber, toAccountNumber, amount, currentTimeStamp));
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return i>0? "Transferred Successfully" : "There is an issue in transferring fund";
	}

	@Override
	public User getUserByAccountNumber(Long accountNumber) {

		return repo.getUserByAccountsAccountNumber(accountNumber);
	}

	@Override
	public UserResponse getUserByMobile(Long mobile) {
		Optional<List<User>> usersOption = repo.findUserByMobile(mobile); 
		if(usersOption.isPresent()) {
			
			List<User> users = usersOption.get();
			List<UserDto> usersDto = new ArrayList<>();
			for(User user : users) {
				UserDto userResponse = new UserDto();
				BeanUtils.copyProperties(user, userResponse);
				usersDto.add(userResponse);
			}
			
			UserResponse response = new UserResponse(HttpStatus.OK, "User Found", usersDto);
			
			return response;
		}
		
		return new UserResponse(HttpStatus.OK, "No user registered with mobile number "+ mobile, null);
	}
	
	

}
